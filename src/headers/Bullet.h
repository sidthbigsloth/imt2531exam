#pragma once
#include "GameObject.h"

class Bullet : public GameObject
{
private: 
    float timeSinceShot = 0.0f;

    glm::vec3 dir;  //direction of bullet
    GameObject* target; //target of bullet, a bullet can only hit intended target (these are smart bullets)
public:
    float speed;    //speed of the bullet
    bool active = false;    //if the bullet is active

    virtual void update();  //updates bullet
    void shoot(glm::vec3 pos, glm::vec3 direction, GameObject* t);  //shoots a bullet from a position in a direction with an intended target;
};