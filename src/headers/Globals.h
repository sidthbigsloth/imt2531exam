#pragma once
#include "src/headers/Camera.h"
#include "Enums.h"
#include "Glider.h"
#include "AIGlider.h"
#include "Bullet.h"

const float RAD = 0.0174533f;   //one degree in radians
const float ROTATE = 1.0f;      //how much to rotate per rotation, in degrees
const float SENSITIVITY = 100;  //sensitivity of mouse
const float BULLETSPEED = 6.0f; //speed of bullets
const int BULLETS = 200;        //amount of bullets
const int PLAYERHEALTH = 200;   //player starting health
const int AIHEALTH = 200;       //AI starting health
const int KEYS = 349;           //amount of keys

extern double g_deltatime;
extern double timeSinceChange;  //time since last season change
extern bool smoothing;          //if smoothing is enabled
extern bool autoSeason;         //if season should auto change
extern bool autoTime;           //if time should auto change
extern bool follow;             //if camera should follow
extern bool pressed[KEYS];      //if keys are pressed
extern int keyAction[KEYS];     //pressed keys associated actions
extern int SCREEN_WIDTH;        
extern int SCREEN_HEIGHT;
extern int accuracy;            //accuracy of AI, how many times it should refine the calculation

extern glm::vec3 lightDist;

extern GLFWwindow* g_window;

extern Camera* mainCamera;
extern Glider* glider;              //player
extern AIGlider* opponent;          //AI
extern Bullet* bullets[BULLETS];    //all the bullets

extern Season season;
extern DayTime time;