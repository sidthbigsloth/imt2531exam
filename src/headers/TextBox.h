#pragma once
#include "GameObject.h"
#include "Enums.h"

#include <SDL2_ttf/include/SDL_ttf.h>


class TextBox : public GameObject
{
public:
    TTF_Font * font;
    std::string text;
    std::string prevText;
    SDL_Color color;
    Allignment allignment;

    TextBox(TTF_Font* fnt, std::string text, SDL_Color color, glm::vec3 pos, Allignment allign, Shader* shader);
    void update();
    void createText();
};