#pragma once
#include "GameObject.h"

class Glider : public GameObject
{
private:
    float minSpeed = 0.5f;          //minimum speed
    float maxSpeed = 5;             //maximum speed
    float timeSinceShot = 0.0f;     //time since last shot was made
    int startPos = 0;               //current starting position
    const int MAXSTARTPOINTS = 5;   //max starting positions
public:
    Glider();
    int health;         //amount of health
    float speed = .5f;  //current speed
    glm::vec3 dir;      //forward direction
    glm::vec3 up;       //up direction
    glm::vec3 right;    //right direction

    virtual void update();  
    virtual void input();   //handles input
    void restartPos();      //restarts position
    virtual void hit();     //is called when glider is hit
};
