#pragma once
#include "GameObject.h"

class AIGlider : public GameObject
{
private:
    float minSpeed = 0.5f;  //minimum speed of AI
    float maxSpeed = 5.0f;  //maximum speed of AI
    float speed = .5f;      //current speed of AI
    float timeSinceShot = 0.0;  //time since last shot was made

    glm::vec3 dir;              //forward direction
    glm::vec3 upDir;            //up direction
    glm::vec3 rightDir;         //right direction
    glm::vec3 playerPos;        //position of player
    glm::vec3 aimPos;           //where to aim
    glm::vec3 playerDir;        //forward direction for player
    glm::vec3 playerUp;         //up direction for player
    glm::vec3 playerRight;      //right direction for player
    glm::vec3 lastPlayerDir;    //forward direction for player last frame
    glm::vec3 lastPlayerUp;     //up direction for player last frame
    glm::vec3 lastPlayerRight;  //right direction for player last frame

    void findPlayer();      //finds player and saves numbers
    void aimForPlayer();    //aims for the player
    void shoot();           //shoots at the player

    void pitch(bool up);    //pitches up or down
    void yaw(bool right);   //yaws right or left
    void roll(bool right);  //rolls right or left
public:
    int health;     //health of AI
    bool canShoot; //if AI is allowed to shoot

    AIGlider();     //constructor
        
    virtual void update();  //does everything
    void restartPos(int pos);   //restarts position depending on players restart position
    virtual void hit();     //is called when AI is hit
};