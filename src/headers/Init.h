﻿#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

bool initWindow();
void inputHandler (GLFWwindow* window, int key, int scancode, int action, int mods);    //handles input
void mouseHandler(GLFWwindow* window, double x, double y);                              //handles input from mouse
void windowsize_callback(GLFWwindow* window, int width, int height);
