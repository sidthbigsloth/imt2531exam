#pragma once
#include <vector>
#include "src/headers/GameObject.h"
#include "src/headers/objectLoader.hpp"

extern std::vector<Light*> lights;              //all the lights
extern std::vector<GameObject*> gameObjects;    //all initiated gameobjects
extern std::vector<Mesh*> meshes;               //all meshes
extern std::vector<Texture*> textures;          //all textures
extern std::vector<Shader*> shaders;            //all shaders

Light* CreateLight(glm::vec3 pos, glm::vec3 color = glm::vec3(1.0f));           //creates a light
void InitGameObject(GameObject* go, Mesh* obj, Texture* tex, Shader* shade);    //initiates a game object
Mesh* LoadObject(std::string fileName);         //loads an object
Texture* LoadTexture(std::string fileName);     //loads a texture
Shader* LoadShader(std::string fileName);       //loads a shader

void UpdateGameObjects();   //updates
void DrawGameObjects();     //draws
void HandleInput();         //handles input
