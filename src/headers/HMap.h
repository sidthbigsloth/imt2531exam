#pragma once

#include <string>
#include "src/headers/Mesh.hpp"

struct Triangle		//	Used to represet faces
{
	glm::vec3 A;
	glm::vec3 B;
	glm::vec3 C;
};

class Hmap 
{
public:
	int height, width, nrChannels;	//	Some misc info about the image
	float amplitude = 255.0f;		//	For adjusting altitude to within 0 and 1

	Mesh* terrain;
	Mesh* water;

	void GenerateHMap(Mesh &mesh, const char* path);
	void GenerateHWater(Mesh &mesh, glm::vec2 size);

private:
	glm::vec3 FaceNormal(Triangle tri);
	glm::vec3 VertexNormal(GLfloat* values, glm::vec2 &size, glm::vec2 &pos);

	Mesh ValuesToMesh(GLfloat* heightValues, glm::vec2 size);
	void SmoothValues(GLfloat* values, glm::vec2 size);
	void Squish(Mesh &values);

};

