#include "src/headers/AIGlider.h"
#include "src/headers/Globals.h"
#include "glm/gtx/rotate_vector.hpp"
#include "src/headers/logger.h"

AIGlider::AIGlider()
{
    //  Default constructor
}

void AIGlider::update()
{   //runs all functions to find, aim for and shoot the player
    timeSinceShot += g_deltatime;
    transform.position += (float(speed * g_deltatime)) * dir;
    findPlayer();
    aimForPlayer();
    if (canShoot && timeSinceShot > 0.25)   //can shoot 4 times per second
        shoot();
}

void AIGlider::findPlayer()
{
    lastPlayerDir = playerDir;
    lastPlayerRight = playerRight;
    lastPlayerUp = playerUp;
    playerUp = glider->up;
    playerDir = glider->dir;
    playerRight = glider->right;
    playerPos = glider->transform.position;

    if ((glider->speed - speed) > 0.3)  //allways keeps the speed lower than the player, why? don't know, felt right
        speed += 0.01f;
    else if ((glider->speed - speed) < 0.3)
        speed -= 0.0f;
}

void AIGlider::aimForPlayer()
{
    float time = 0;
    glm::vec3 newPos = playerPos;

    for (int i = 0; i < accuracy; i++)
    {   //calculates players position when bullet reaches players position
        time = glm::length(newPos - transform.position) / BULLETSPEED;
        newPos = playerPos + (playerDir * glider->speed * time);
    }

    //sets position to aim for
    aimPos = newPos;
    aimPos -= transform.position;
    aimPos = glm::normalize(aimPos);

    glm::vec3 upDiff = aimPos * upDir;          //difference in up direction
    glm::vec3 sideDiff = aimPos * rightDir;     //difference in side direction
    glm::vec3 frontDiff = aimPos * dir;         //difference in forward direction, used to determine if player is ahead or behind
    glm::vec3 rollDiff = playerUp * rightDir;   //difference in roll, used to have the ability to do same maneuvers as player
    glm::vec3 upDirDiff = playerUp * upDir;     //difference in up vectors, used to determine if player and AI are upside down compared to each other
    float l = 0;
    
    l = upDiff.x + upDiff.y + upDiff.z;
    if (l < -0.001 || l > 0.001)    //l is positive if player is above on AI
        pitch(l > 0.001);

    l = sideDiff.x + sideDiff.y + sideDiff.z;
    if (l < -0.001 || l > 0.001)    //l is positive if player is right of AI
        yaw(l > 0.001);
    
    l = rollDiff.x + rollDiff.y + rollDiff.z;
    if (l < -0.001 || l > 0.001)    //l is positive if players roll is closer on the right
        roll(l > 0.001);

    l = upDirDiff.x + upDirDiff.y + upDirDiff.z;
    if (l < -0.9f)  //l is close to -1 when player is upside down compared to AI
        roll(true); //no direction is closer, panics rolls to right and knows what to do from there

    l = frontDiff.x + frontDiff.y + frontDiff.z;
    if (l < -0.9)   //l is close to -1 when player is directly behind AI
    {   //no way is better to turn, panics move on all axis and knows what to do from there
        pitch(true);
        yaw(true);
        roll(true);
    }
}

void AIGlider::shoot()
{
    glm::vec3 aimDiff = aimPos * dir;
    float l = aimDiff.x + aimDiff.y + aimDiff.z;
    if (l > 0.999)  //l is close to 1 when player is directly ahead of AI
    {
        bullets[0]->shoot(transform.position, dir, glider); //shoots a bullet with the player as a target
        timeSinceShot = 0.0f;
    }
}

void AIGlider::pitch(bool up)
{   //pitches up or down
    if (up)
    {   //rotates forward and up around right direction
        dir = glm::rotate(dir, ROTATE * RAD, rightDir);
        dir = glm::normalize(dir);
        upDir = glm::rotate(upDir, ROTATE * RAD, rightDir);
        upDir = glm::normalize(upDir);
        transform.rotate(-ROTATE, glm::vec3(0.0f, 0.0f, 1.0f), 1);
    }
    else
    {   //rotates forward and up around right direction
        dir = glm::rotate(dir, -(ROTATE * RAD), rightDir);
        dir = glm::normalize(dir);
        upDir = glm::rotate(upDir, -(ROTATE * RAD), rightDir);
        upDir = glm::normalize(upDir);
        transform.rotate(ROTATE, glm::vec3(0.0f, 0.0f, 1.0f), 1);
    }
}

void AIGlider::yaw(bool right)
{   //yaws right or left
    if (right)
    {   //rotates forward and right around up direction
        dir = glm::rotate(dir, -(ROTATE * RAD), upDir);
        dir = glm::normalize(dir);
        rightDir = glm::rotate(rightDir, -(ROTATE * RAD), upDir);
        rightDir = glm::normalize(rightDir);
        transform.rotate(-ROTATE, glm::vec3(0.0f, 1.0f, 0.0f), 1);
    }
    else
    {   //rotates forward and right around up direction
        dir = glm::rotate(dir, ROTATE * RAD, upDir);
        dir = glm::normalize(dir);
        rightDir = glm::rotate(rightDir, ROTATE * RAD, upDir);
        rightDir = glm::normalize(rightDir);
        transform.rotate(ROTATE, glm::vec3(0.0f, 1.0f, 0.0f), 1);
    }
}

void AIGlider::roll(bool right)
{   //rolls right or left
    if (right)
    {   //rotates up and right direction around forward direction
        upDir = glm::rotate(upDir, ROTATE * RAD, dir);
        upDir = glm::normalize(upDir);
        rightDir = glm::rotate(rightDir, ROTATE * RAD, dir);
        rightDir = glm::normalize(rightDir);
        transform.rotate(-ROTATE, glm::vec3(1.0f, 0.0f, 0.0f), 1);
    }
    else
    {   //rotates up and right direction around forward direction
        upDir = glm::rotate(upDir, -(ROTATE * RAD), dir);
        upDir = glm::normalize(upDir);
        rightDir = glm::rotate(rightDir, -(ROTATE * RAD), dir);
        rightDir = glm::normalize(rightDir);
        transform.rotate(ROTATE, glm::vec3(1.0f, 0.0f, 0.0f), 1);
    }
}

void AIGlider::restartPos(int pos)
{   //restarts position and health
    transform.rotation = glm::mat4(1.0f);
    health = AIHEALTH;

    switch (pos)
    {
    case 0:
        transform.position = glm::vec3(-5.0f, 10.0f, 10.0f);
        dir = glm::vec3(0.0f, 0.0f, -1.0f);
        upDir = glm::vec3(0.0f, 1.0f, 0.0f);
        rightDir = glm::vec3(1.0f, 0.0f, 0.0f);
        transform.rotate(-90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    case 1:
        transform.position = glm::vec3(0.0f, 10.0f, 0.0f);
        dir = glm::vec3(0.0f, 0.0f, 1.0f);
        upDir = glm::vec3(0.0f, 1.0f, 0.0f);
        rightDir = glm::vec3(-1.0f, 0.0f, 0.0f);
        transform.rotate(90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    case 2:
        transform.position = glm::vec3(-5.0f, 10.0f, -10.0f);
        dir = glm::vec3(0.0f, 0.0f, 1.0f);
        upDir = glm::vec3(0.0f, 1.0f, 0.0f);
        rightDir = glm::vec3(-1.0f, 0.0f, 0.0f);
        transform.rotate(90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    case 3:
        transform.position = glm::vec3(5.0f, 10.0f, 10.0f);
        dir = glm::vec3(0.0f, 0.0f, -1.0f);
        upDir = glm::vec3(0.0f, 1.0f, 0.0f);
        rightDir = glm::vec3(1.0f, 0.0f, 0.0f);
        transform.rotate(-90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    case 4:
        transform.position = glm::vec3(5.0f, 10.0f, -10.0f);
        dir = glm::vec3(0.0f, 0.0f, 1.0f);
        upDir = glm::vec3(0.0f, 1.0f, 0.0f);
        rightDir = glm::vec3(-1.0f, 0.0f, 0.0f);
        transform.rotate(90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    }
}

void AIGlider::hit()
{   //decrements health when hit
    health--;
}