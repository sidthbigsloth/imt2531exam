#include "src/headers/Functions.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Init.h"
#include "src/headers/Globals.h"
#include "src/headers/Camera.h"
#include "src/headers/Glider.h"
#include "src/headers/TextBox.h"
#include "src/headers/HMap.h"

#include "glm/glm.hpp"
#include "glm/gtx/quaternion.hpp"

#define AUDIO_IMPLEMENTATION
#include "src/headers/Audio.hpp"

GameObject* sun;

Mesh* sunMesh;
Mesh* mapMesh;
Mesh* gliderMesh;
Mesh* bulletMesh;

Shader* heightShader;

TextBox* seasonText;
TextBox* timeText;
TextBox* speedText;
TextBox* healthText;
TextBox* winText;

float timeSinceWin = 10;

//--------------------------------------------------------------------
//	USED TO INITIALIZE THE GAME AND YOUR GAMEOBJECTS, CLASSES, AND FUNCTIONS
//--------------------------------------------------------------------
void Start()
{
    //inits and loads background music
    Audio::InitAudio();
    Audio::AddBGM("../resources/sound/pacman_jazz.wav", "bgm", false);
	
    //loads meshes
    sunMesh = LoadObject("cherry");
    gliderMesh = LoadObject("glider");
    bulletMesh = LoadObject("watermelon");

    //loads heightmap mesh
    mapMesh = new Mesh;
    Hmap hMap;
    hMap.GenerateHMap(*mapMesh, "../resources/heightmap/height100.png");

    //loads textures
	Texture* tex = LoadTexture("white");
    Texture* gliderTex = LoadTexture("texture");
    Texture* melon = LoadTexture("watermelon");

    //loads shaders
	Shader* standardShader = LoadShader("standard");
    Shader* textShader = LoadShader("text");
    heightShader = LoadShader("height");

    //sets values of heightmap shader
    heightShader->setFloat("mountainLine", .21);
    heightShader->setVec4("snow", glm::vec4(1.0, 1.0, 1.0, 1.0));
    heightShader->setVec4("mountain", glm::vec4(0.8, 0.75, 0.65, 1.0));

    //makes the camera
    mainCamera = new Camera(glm::vec3(0, 0, 5), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0), freeCam);

    // Loads the font
	TTF_Font* font = TTF_OpenFont("../resources/fonts/Joystix.TTF", 20);     

    //initiates the text boxes
    seasonText = new TextBox(font, "season", { 255, 255, 255, 255 }, glm::vec3(-0.4, 0.3, 0), right, textShader);
    seasonText->transform.scale = glm::vec3(.07f);
    timeText = new TextBox(font, "daylight", { 255, 255, 255, 255 }, glm::vec3(0.7, 0.3, 0), right, textShader);
    timeText->transform.scale = glm::vec3(.07f);
    speedText = new TextBox(font, "speed", { 255, 255, 255, 255 }, glm::vec3(-0.7, -0.4, 0), left, textShader);
    speedText->transform.scale = glm::vec3(.07f);
    healthText = new TextBox(font, "health", { 255, 255, 255, 255 }, glm::vec3(0.7, -0.4, 0), right, textShader);
    healthText->transform.scale = glm::vec3(.07f);
    winText = new TextBox(font, "win", { 255, 255, 255, 255 }, glm::vec3(0.0, 0.3, 0), center, textShader);
    winText->transform.scale = glm::vec3(.07f);

    //makes the game objects
    sun = new GameObject();
    glider = new Glider();
    opponent = new AIGlider();
    GameObject* mapOBJ = new GameObject();

    //inits all game objects
	InitGameObject(sun, sunMesh, tex, standardShader);
    InitGameObject(glider, gliderMesh, gliderTex, standardShader);
    InitGameObject(opponent, gliderMesh, gliderTex, standardShader);
    InitGameObject(mapOBJ, mapMesh, tex, heightShader);
    InitGameObject(mainCamera, nullptr, nullptr, nullptr);
    
    //inits all the bullets
    for (int i = 0; i < BULLETS; i++)
    {
        bullets[i] = new Bullet();
        bullets[i]->mesh = bulletMesh;
        bullets[i]->material.shader = standardShader;
        bullets[i]->material.texture = melon;
        bullets[i]->transform.scale = glm::vec3(0.05);
        bullets[i]->speed = BULLETSPEED;
    }

    //scales things to better sizes
    glider->transform.scale = glm::vec3(0.03f);
    opponent->transform.scale = glm::vec3(0.03f);
    mapOBJ->transform.scale = glm::vec3(250.0f, 50.0f, 500.0f);

    //resets position of glider
    glider->restartPos();

    //makes the light
	Light* light = CreateLight(glm::vec3(0.0f));
	
    //sets the cameras position and right direction
    mainCamera->setRightDirection(glm::vec3(1, 0, 0));
    mainCamera->transform.position = glm::vec3(0, 10, 0);

    //plays musuc
    Audio::PlayBGM("bgm", -1);
}

//--------------------------------------------------------------------
//	USED TO UPDATE THE GAME, OTHERWISE THE SAME AS START
//--------------------------------------------------------------------
void Update()
{
    std::string s = ""; //season text
    std::string t = ""; //time text
    std::string w = ""; //win text

    //turns on or of smoothing
    heightShader->setInt("smoothing", (smoothing ? 1 : 0));

    //increments times
    timeSinceChange += g_deltatime;
    timeSinceWin += g_deltatime;

    if (autoSeason && timeSinceChange > 2)
    {   //changes season automatically
        switch (season)
        {
        case SUMMER:
            season = AUTUMN;
            break;
        case AUTUMN:
            season = WINTER;
            break;
        case WINTER:
            season = SPRING;
            break;
        case SPRING:
            season = SUMMER;
            break;
        }
        timeSinceChange = 0;
    }

    switch (season)
    {   //sets shader values based on season
    case SUMMER:
        heightShader->setVec4("water", glm::vec4(0.0, 0.0, 1.0, 1.0));
        heightShader->setVec4("grass", glm::vec4(0.1, 0.9, 0.1, 1.0));
        heightShader->setFloat("snowLine", 0.25);
        heightShader->setFloat("waterLine", 0.08);
        s = "summer";
        break;
    case AUTUMN:
        heightShader->setVec4("water", glm::vec4(0.0, 0.0, 1.0, 1.0));
        heightShader->setVec4("grass", glm::vec4(0.82f, 0.41f, 0.12f, 1.0));
        heightShader->setFloat("snowLine", .23);
        heightShader->setFloat("waterLine", 0.075);
        s = "autumn";
        break;
    case WINTER:
        heightShader->setVec4("water", glm::vec4(0.5, 0.8, 1.0, 1.0));
        heightShader->setVec4("grass", glm::vec4(0.5, 0.8, 0.3, 1.0));
        heightShader->setFloat("snowLine", .21);
        heightShader->setFloat("waterLine", 0.07);
        s = "winter";
        break;
    case SPRING:
        heightShader->setVec4("water", glm::vec4(0.0, 0.0, 1.0, 1.0));
        heightShader->setVec4("grass", glm::vec4(0.0, 1.0, 0.0, 1.0));
        heightShader->setFloat("snowLine", .23);
        heightShader->setFloat("waterLine", 0.075);
        s = "spring";
        break;
    }

    if (autoTime)
    {   //moves light automatically
        sun->transform.position = glm::vec3(0.0f, 0.0f, 0.0f);
        sun->transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(20.0f) * g_deltatime * 1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        lightDist = sun->transform.rotation * glm::vec4(lightDist, 1.0f);
        sun->transform.position = glm::vec3(1.0f) + lightDist;
    }
    else
    {
        switch (time)
        {   //sets light depending on time of day
        case DAWN:
            sun->transform.position = glm::vec3(0.0f);
            sun->transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(270.0f)), glm::vec3(0.0f, 0.0f, 1.0f));
            lightDist = sun->transform.rotation * glm::vec4(0.0f, 100.0f, 0.f, 0.0f);
            sun->transform.position = glm::vec3(1.0f) + lightDist;
            break;
        case NOON:
            sun->transform.position = glm::vec3(0.0f);
            sun->transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(0.0f)), glm::vec3(0.0f, 0.0f, 1.0f));
            lightDist = sun->transform.rotation * glm::vec4(0.0f, 100.0f, 0.f, 0.0f);
            sun->transform.position = glm::vec3(1.0f) + lightDist;
            break;
        case DUSK:
            sun->transform.position = glm::vec3(0.0f);
            sun->transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(90.0f)), glm::vec3(0.0f, 0.0f, 1.0f));
            lightDist = sun->transform.rotation * glm::vec4(0.0f, 100.0f, 0.f, 0.0f);
            sun->transform.position = glm::vec3(1.0f) + lightDist;
            break;
        case NIGHT:
            sun->transform.position = glm::vec3(0.0f);
            sun->transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(180.0f)), glm::vec3(0.0f, 0.0f, 1.0f));
            lightDist = sun->transform.rotation * glm::vec4(0.0f, 100.0f, 0.f, 0.0f);
            sun->transform.position = glm::vec3(1.0f) + lightDist;
            break;
        }
    }

    //sets time text based on light position
    if (sun->transform.position.x > 50)
        t = "dawn";
    if (sun->transform.position.x < -50)
        t = "dusk";
    if (sun->transform.position.y > 50)
        t = "noon";    
    if (sun->transform.position.y < -50)
        t = "night";

    //updates bullets
    for (int i = 0; i < BULLETS; i++)
        bullets[i]->update();

    //checks if player or AI is dead
    if (glider->health < 1 || opponent->health < 1)
    {
        if (glider->health < 1)
        {
            w = "You lose!";
        }
        else
        {
            w = "You won!";
        }
        glider->restartPos();
        timeSinceWin = 0;
    }

    lights[0]->transform.position = sun->transform.position;
    
    //prints season
    seasonText->text = s;
    seasonText->update();
    seasonText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)));

    //prints time
    timeText->text = t;
    timeText->update();
    timeText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)));

    //prints speed
    speedText->text = "Speed: " + std::to_string(glider->speed);
    speedText->update();
    speedText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)));

    //prints player health
    healthText->text = "Health: " + std::to_string(glider->health);
    healthText->update();
    healthText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)));

    //prints win text for 2 seconds after victory or defeat
    if (timeSinceWin < 2)
    {
        winText->text = w;
        winText->update();
        winText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
    }
    else
    {
        winText->text = "";
        winText->update();
        winText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 10.0f)));
    }
}
