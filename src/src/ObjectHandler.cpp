#include "src/headers/ObjectHandler.h"
#include "src/headers/mad.h"
#include "src/headers/Globals.h"

std::vector<Light*> lights;
std::vector<GameObject*> gameObjects;
std::vector<Mesh*> meshes;
std::vector<Texture*> textures;
std::vector<Shader*> shaders;

Light* CreateLight(glm::vec3 pos, glm::vec3 color)
{
	Light* light = new Light();
	light->transform.position = pos;
	light->color = color;
	lights.push_back(light);

	return light;
}

void InitGameObject(GameObject* go, Mesh* obj, Texture* tex, Shader* shade)
{
	go->init(obj, tex, shade);
}

Mesh* LoadObject(std::string fileName)
{   //loads a given mesh
	std::string path = "../resources/models/";

	Mesh* object = new Mesh(loadObject(path + fileName + ".obj")[0]);
	meshes.push_back(object);

	return object;
}

Texture* LoadTexture(std::string fileName)
{   //loads a given texture
	std::string path = "../resources/textures/";

	Texture* texture = new Texture((path + fileName + ".png").c_str());
	textures.push_back(texture);

	return texture;
}

Shader* LoadShader(std::string fileName)
{   //loads a given shader
	Shader* shader = new Shader(fileName);
	shaders.push_back(shader);

	return shader;
}

void UpdateGameObjects()
{
    HandleInput();
	for (int i=0; i<gameObjects.size(); i++)
		gameObjects[i]->update();
}

void DrawGameObjects()
{
	for (int i = 0; i<gameObjects.size(); i++)
		gameObjects[i]->draw(mainCamera->getView());
}

void HandleInput()
{
    for (int i = 0; i < KEYS; i++)
    {   //goes through all keys and check if they're pressed
        if (pressed[i])
        {
            switch (i)
            {
            case GLFW_KEY_O:    //toggles smoothing
                if (keyAction[i] == GLFW_PRESS)
                {
                    keyAction[i] = GLFW_RELEASE;
                    smoothing = !smoothing;
                }
                break;

            case GLFW_KEY_5:    //toggles automatic season change
                if (keyAction[i] == GLFW_PRESS)
                {
                    keyAction[i] = GLFW_RELEASE;
                    autoSeason = !autoSeason;
                }
                break;

            case GLFW_KEY_0:    //toggles automatic time change
                if (keyAction[i] == GLFW_PRESS)
                {
                    keyAction[i] = GLFW_RELEASE;
                    autoTime = !autoTime;
                }
                break;

            case GLFW_KEY_1:
                season = SPRING;
                break;

            case GLFW_KEY_2:
                season = SUMMER;
                break;

            case GLFW_KEY_3:
                season = AUTUMN;
                break;

            case GLFW_KEY_4:
                season = WINTER;
                break;

            case GLFW_KEY_6:
                time = DAWN;
                break;

            case GLFW_KEY_7:
                time = NOON;
                break;

            case GLFW_KEY_8:
                time = DUSK;
                break;

            case GLFW_KEY_9:
                time = NIGHT;
                break;

            case GLFW_KEY_Z:    //decreases AI accuracy
                accuracy--;
                if (accuracy < 0)
                    accuracy = 0;
                break;

            case GLFW_KEY_X:    //increases AI accuracy
                accuracy++;
                if (accuracy > 20)
                    accuracy = 20;
                break;

            case GLFW_KEY_C:    //toggles AI's ability to shoot
                if (keyAction[i] == GLFW_PRESS)
                {
                    opponent->canShoot = !opponent->canShoot;
                    keyAction[i] = GLFW_RELEASE;
                }
                break;

            }
        }
    }

    for (int i = 0; i < gameObjects.size(); i++)
        gameObjects[i]->input();
}
