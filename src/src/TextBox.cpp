#include "src/headers/TextBox.h"

TextBox::TextBox(TTF_Font* fnt, std::string txt, SDL_Color clr, glm::vec3 pos, Allignment allign, Shader* shader)
{
    text = txt;
    color = clr;
    allignment = allign;

    font = fnt;//TTF_OpenFont("../resources/fonts/Joystix.TTF", 20);     // Loads the font
    material.texture = new Texture();
    material.shader = shader;
    mesh = new Mesh();
    transform.position = pos;

    createText();
}

void TextBox::update()
{
    if (text != prevText)
        createText();
}

void TextBox::createText()
{
    // Width used for the width of the drawn rectangle
    int width = material.texture->createTextureFromText(font, text, color); // Creates texture from input string

                                                                            // Mesh creation
    std::vector<glm::vec3> vertices;

    // Vertices based on allignment
    if (allignment == center)
    {
        vertices = {
            glm::vec3((width / 20) / 2,  1.0f,  0.0f),  // Top Right
            glm::vec3((width / 20) / 2,  0.0f,  0.0f),  // Bottom Right
            glm::vec3((-width / 20) / 2, 0.0f,  0.0f),  // Bottom Left
            glm::vec3((-width / 20) / 2, 1.0f,  0.0f),  // Top Left
            glm::vec3((width / 20) / 2,  1.0f,  0.0f),  // Top Right
            glm::vec3((-width / 20) / 2, 0.0f,  0.0f),  // Bottom Left
        };
    }
    if (allignment == left)
    {
        vertices = {
            glm::vec3((width / 20),  1.0f,  0.0f),      // Top Right
            glm::vec3((width / 20),  0.0f,  0.0f),      // Bottom Right
            glm::vec3(0.0f, 0.0f,  0.0f),               // Bottom Left
            glm::vec3(0.0f, 1.0f,  0.0f),               // Top Left
            glm::vec3((width / 20),  1.0f,  0.0f),      // Top Right
            glm::vec3(0.0f, 0.0f,  0.0f),               // Bottom Left
        };
    }

    if (allignment == right)
    {
        vertices = {
            glm::vec3(0.0f,  1.0f,  0.0f),              // Top Right
            glm::vec3(0.0f,  0.0f,  0.0f),              // Bottom Right
            glm::vec3((-width / 20), 0.0f,  0.0f),      // Bottom Left
            glm::vec3((-width / 20), 1.0f,  0.0f),      // Top Left
            glm::vec3(0.0f,  1.0f,  0.0f),              // Top Right
            glm::vec3((-width / 20), 0.0f,  0.0f),      // Bottom Left
        };
    }

    // Texture coordinates
    std::vector<glm::vec2> texCoords = {

        glm::vec2(1.0f, 0.0f),
        glm::vec2(1.0f, 1.0f),
        glm::vec2(0.0f, 1.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(1.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),
    };

    mesh->vertices = vertices;
    mesh->texCoord = texCoords;
}