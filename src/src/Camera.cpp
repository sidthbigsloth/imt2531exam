#include "src/headers/Camera.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"
#include "glm/gtx/rotate_vector.hpp"

Camera::Camera(glm::vec3 pos, glm::vec3 viewDir, glm::vec3 upDir, CameraMode newMode)
{
    transform.position = pos;
    lookatDirection = viewDir;
    upDirection = upDir;
    mode = newMode;

    rotationAngle = 45.0f;
    rotationSpeed = 1.0f;
    rotationDirection = glm::vec3(0, 1, 0);
    lookatPosition = glm::vec3(0, 0, 0);
    followDistance = glm::vec3(0, 0, 1);
    
    view = glm::mat4(1.0f);
    projection = glm::perspective(glm::radians(45.0f), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, 1000.0f);
}

void Camera::update()
{
    switch (mode)
    {
    case freeCam:
        view = glm::lookAt(transform.position, transform.position + lookatDirection, upDirection);
        view = view * transform.rotation;
        break;

    case followCam:
        transform.position = target->transform.position + glm::vec3(target->transform.rotation * glm::vec4(followDistance, 1.0f));
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;
    }

}

void Camera::input()
{
    for (int i = 0; i < KEYS; i++)
    {   //goes through all keys and check if they are pressed
        if (pressed[i])
        {   
            if (i == GLFW_KEY_SLASH && keyAction[i] == GLFW_PRESS)
            {   //changes camera mode
                keyAction[i] = GLFW_RELEASE;
                follow = !follow;
                if (follow)
                {
                    mainCamera->setTarget(glider);
                    mainCamera->setCamMode(followCam);
                    mainCamera->setFollowDistance(glm::vec3(2.0f, 0.0f, 0.0f));
                }
                else
                {
                    mainCamera->setCamMode(freeCam);
                    mainCamera->setUpDirection(glm::vec3(0.0f, 1.0f, 0.0f));
                }
            }

            if (mode == freeCam)
            {
                switch (i)
                {
                case GLFW_KEY_N:    //"Zoom in"
                    transform.position += lookatDirection * 0.25f;
                    break;

                case GLFW_KEY_M:    //"Zoom out"
                    transform.position -= lookatDirection * 0.25f;
                    break;

                case GLFW_KEY_I:    //forward
                    transform.position += 0.15f*lookatDirection;
                    break;

                case GLFW_KEY_K:    //back
                    transform.position -= 0.15f*lookatDirection;
                    break;

                case GLFW_KEY_J:    //left
                    transform.position -= 0.15f*rightDirection;
                    break;

                case GLFW_KEY_L:    //right
                    transform.position += 0.15f*rightDirection;
                    break;

                case GLFW_KEY_Y:    //up
                    transform.position.y += 0.15f;
                    break;

                case GLFW_KEY_H:    //down
                    transform.position.y -= 0.15f;
                    break;

                case GLFW_KEY_SEMICOLON:    //rotate left
                    upDirection = glm::rotate(upDirection, -RAD, lookatDirection);
                    upDirection = glm::normalize(upDirection);
                    rightDirection = glm::rotate(rightDirection, -RAD, lookatDirection);
                    rightDirection = glm::normalize(rightDirection);
                    break;

                case GLFW_KEY_APOSTROPHE:   //rotate right
                    upDirection = glm::rotate(upDirection, RAD, lookatDirection);
                    upDirection = glm::normalize(upDirection);
                    rightDirection = glm::rotate(rightDirection, RAD, lookatDirection);
                    rightDirection = glm::normalize(rightDirection);
                    break;
                }
            }
        }
    }
}

void Camera::mouseInput(GLFWwindow* window, double x, double y)
{
    if (mode == freeCam) //moves camera based on mouse position
    {
        float deltaX = 0, deltaY = 0;
        if(lastMouseX > 0 && lastMouseX < SCREEN_WIDTH)
            deltaX = lastMouseX - x;
        if(lastMouseY > 0 && lastMouseY < SCREEN_HEIGHT)
            deltaY = lastMouseY - y;

        lookatDirection = glm::rotate(lookatDirection, deltaX / SENSITIVITY, upDirection);
        lookatDirection = glm::normalize(lookatDirection);
        rightDirection = glm::rotate(rightDirection, deltaX / SENSITIVITY, upDirection);
        rightDirection = glm::normalize(rightDirection);

        lookatDirection = glm::rotate(lookatDirection, deltaY / SENSITIVITY, rightDirection);
        lookatDirection = glm::normalize(lookatDirection);
        upDirection = glm::rotate(upDirection, deltaY / SENSITIVITY, rightDirection);
        upDirection = glm::normalize(upDirection);

        lastMouseX = x;
        lastMouseY = y;
    }
}

void Camera::rotate(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
}

void Camera::tilt(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    upDirection = transform.rotation * glm::vec4(upDirection, 1.0);
}

void Camera::setCamMode(CameraMode newMode)
{
    mode = newMode;
}

void Camera::setFollowDistance(glm::vec3 newDistance)
{
    followDistance = newDistance;
}

void Camera::setUpDirection(glm::vec3 newDirection)
{
    upDirection = newDirection;
}

void Camera::setLookatDirection(glm::vec3 newDirection)
{
    lookatDirection = newDirection;
}

void Camera::setRightDirection(glm::vec3 newDirection)
{
    rightDirection = newDirection;
}

void Camera::setLookatPosition(glm::vec3 newPosition)
{
    lookatPosition = newPosition;
}

void Camera::setTarget(GameObject* newTarget)
{
    target = newTarget;
}

void Camera::setRotationDirection(glm::vec3 newDirection)
{
    rotationDirection = newDirection;
}

void Camera::setRotationAngle(float angle)
{
    rotationAngle = angle;
}

void Camera::setRotationSpeed(float speed)
{
    rotationSpeed = speed;
}

void Camera::updateProjection(int width, int height)
{
    projection = glm::perspective(glm::radians(45.0f), float(width) / float(height), 0.1f, 100.f);
}

glm::vec3 Camera::getFollowDistance()
{
    return followDistance;
}

glm::vec3 Camera::getUpDirection()
{
    return upDirection;
}

glm::vec3 Camera::getLookatDirection()
{
    return lookatDirection;
}

glm::vec3 Camera::getRightDirection()
{
    return rightDirection;
}

glm::vec3 Camera::getLookatPosition()
{
    return lookatPosition;
}

glm::mat4 Camera::getProjection()
{
    return projection;
}

glm::mat4 Camera::getView()
{
    return view;
}

GameObject* Camera::getTarget()
{
    return target;
}