#include "src/headers/Globals.h"

double g_deltatime;
double timeSinceChange = true; 
bool smoothing = true;
bool autoSeason = true;
bool autoTime = true;
bool follow = false;
bool pressed[KEYS];
int keyAction[KEYS];
int SCREEN_WIDTH = 600;
int SCREEN_HEIGHT = 600;
int accuracy = 10;

glm::vec3 lightDist(0.0f, 100.0f, 0.f);

GLFWwindow* g_window;

Camera* mainCamera;
Glider* glider;
AIGlider* opponent;
Bullet* bullets[BULLETS];

Season season = SUMMER;
DayTime time = DAWN;