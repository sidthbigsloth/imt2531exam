#include "src/headers/Bullet.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"

void Bullet::update()
{   //updates the bullets position and checks for a hit if active
    if (active)
    {
        timeSinceShot += g_deltatime;
        transform.position += dir * (speed * float(g_deltatime));

        if (timeSinceShot > 50)
            active = false;

        draw(mainCamera->getView());

        glm::vec3 targetDiff = target->transform.position - transform.position;
        float l = targetDiff.x + targetDiff.y + targetDiff.z;
        l = glm::length(targetDiff);

        if (l < 0.1)
        {
            target->hit();
            active = false;
        }
    }
}

void Bullet::shoot(glm::vec3 pos, glm::vec3 direction, GameObject* t)
{   //shoots a bullet from a position in a direction with an intended target
    transform.position = pos;
    dir = direction;
    active = true;
    target = t;
    timeSinceShot = 0.0f;

    //moves the bullet to the back
    for (int i = 1; i < BULLETS; i++)
    {
        bullets[i - 1] = bullets[i];
    }
    bullets[BULLETS - 1] = this;
}