#pragma once
#include "src/headers/Glider.h"
#include "src/headers/Globals.h"
#include "src/headers/ObjectHandler.h"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "src/headers/logger.h"
#include <string>

Glider::Glider()
{
    //  Default constructor
}

void Glider::update()
{   //updates position and cameras up direction if it is following
    transform.position += (float(speed * g_deltatime) * dir);
    timeSinceShot += g_deltatime;

    if(follow)
        mainCamera->setUpDirection(up);
}

void Glider::input()
{
    for (int i = 0; i < KEYS; i++)
    {   //goes through all keys and check if they are pressed
        if (pressed[i])
        {
            switch (i)
            {
            case GLFW_KEY_W:    //tips nose down
                dir = glm::rotate(dir, -(ROTATE * RAD), right);
                dir = glm::normalize(dir);
                up = glm::rotate(up, -(ROTATE * RAD), right);
                up = glm::normalize(up);
                transform.rotate(ROTATE, glm::vec3(0.0f, 0.0f, 1.0f), 1);
                break;

            case GLFW_KEY_S:    //tips nose up
                dir = glm::rotate(dir, ROTATE * RAD, right);
                dir = glm::normalize(dir);
                up = glm::rotate(up, ROTATE * RAD, right);
                up = glm::normalize(up);
                transform.rotate(-ROTATE, glm::vec3(0.0f, 0.0f, 1.0f), 1);
                break;

            case GLFW_KEY_A:    //rolls left
                up = glm::rotate(up, -(ROTATE * RAD), dir);
                up = glm::normalize(up);
                right = glm::rotate(right, -(ROTATE * RAD), dir);
                right = glm::normalize(right);
                transform.rotate(ROTATE, glm::vec3(1.0f, 0.0f, 0.0f), 1);
                break;

            case GLFW_KEY_D:    //rolls right
                up = glm::rotate(up, ROTATE * RAD, dir);
                up = glm::normalize(up);
                right = glm::rotate(right, ROTATE * RAD, dir);
                right = glm::normalize(right);
                transform.rotate(-ROTATE, glm::vec3(1.0f, 0.0f, 0.0f), 1);
                break;

            case GLFW_KEY_Q:    //yaw left
                dir = glm::rotate(dir, ROTATE * RAD, up);
                dir = glm::normalize(dir);
                right = glm::rotate(right, ROTATE * RAD, up);
                right = glm::normalize(right);
                transform.rotate(ROTATE, glm::vec3(0.0f, 1.0f, 0.0f), 1);
                break;

            case GLFW_KEY_E:    //yaw right
                dir = glm::rotate(dir, -(ROTATE * RAD), up);
                dir = glm::normalize(dir);
                right = glm::rotate(right, -(ROTATE * RAD), up);
                right = glm::normalize(right);
                transform.rotate(-ROTATE, glm::vec3(0.0f, 1.0f, 0.0f), 1);
                break;

            case GLFW_KEY_COMMA:    //speed down
                speed -= 0.01;
                if (speed < minSpeed)
                    speed = minSpeed;
                break;

            case GLFW_KEY_PERIOD:   //speed up
                speed += 0.01;
                if (speed > maxSpeed)
                    speed = maxSpeed;
                break;

            case GLFW_KEY_R:    //reset position
                if (keyAction[i] == GLFW_PRESS)
                {
                    keyAction[i] = GLFW_RELEASE;
                    restartPos();
                }
                break;

            case GLFW_KEY_F:    //change reset position and reset
                if (keyAction[i] == GLFW_PRESS)
                {
                    keyAction[i] = GLFW_RELEASE;
                    startPos++;
                    if (startPos >= MAXSTARTPOINTS)
                        startPos = 0;
                    restartPos();
                }
                break;

            case GLFW_KEY_SPACE:    //shoot
                if (timeSinceShot > 0.25f)
                {
                    bullets[0]->shoot(transform.position, dir, opponent);
                    timeSinceShot = 0.0f;
                }
                break;

            }
        }
    }
}

void Glider::restartPos()
{   //restarts position and rotation and health
    transform.rotation = glm::mat4(1.0f);
    opponent->restartPos(startPos); //restarts opponent
    health = PLAYERHEALTH;

    for (int i = 0; i < BULLETS; i++)
    {   //resets bullets
        bullets[i]->active = false;
    }

    switch (startPos)
    {
    case 0:
        transform.position = glm::vec3(0.0f, 10.0f, 0.0f);
        dir = glm::vec3(0.0f, 0.0f, 1.0f);
        up = glm::vec3(0.0f, 1.0f, 0.0f);
        right = glm::vec3(-1.0f, 0.0f, 0.0f);
        transform.rotate(90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    case 1:
        transform.position = glm::vec3(-5.0f, 10.0f, -10.0f);
        dir = glm::vec3(0.0f, 0.0f, 1.0f);
        up = glm::vec3(0.0f, 1.0f, 0.0f);
        right = glm::vec3(-1.0f, 0.0f, 0.0f);
        transform.rotate(90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    case 2:
        transform.position = glm::vec3(5.0f, 10.0f, 10.0f);
        dir = glm::vec3(0.0f, 0.0f, -1.0f);
        up = glm::vec3(0.0f, 1.0f, 0.0f);
        right = glm::vec3(1.0f, 0.0f, 0.0f);
        transform.rotate(-90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    case 3:
        transform.position = glm::vec3(5.0f, 10.0f, -10.0f);
        dir = glm::vec3(0.0f, 0.0f, 1.0f);
        up = glm::vec3(0.0f, 1.0f, 0.0f);
        right = glm::vec3(-1.0f, 0.0f, 0.0f);
        transform.rotate(90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    case 4:
        transform.position = glm::vec3(-5.0f, 10.0f, 10.0f);
        dir = glm::vec3(0.0f, 0.0f, -1.0f);
        up = glm::vec3(0.0f, 1.0f, 0.0f);
        right = glm::vec3(1.0f, 0.0f, 0.0f);
        transform.rotate(-90, glm::vec3(0.0f, 1.0f, 0.0f), 1);
        break;

    }
}

void Glider::hit()
{   //decrements health
    health--;
}
