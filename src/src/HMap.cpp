#include "src/headers/HMap.h"
#include "src/headers/stb_image.h"


glm::vec3 Hmap::FaceNormal(Triangle tri)	//	Calculates the normal of a face
{
	return glm::cross(tri.B - tri.A, tri.C - tri.A);
}

glm::vec3 Hmap::VertexNormal(GLfloat* values, glm::vec2 &size, glm::vec2 &pos)	//	Calculates the vertex normal of a vertex, pos is the position in values
{
	//	This could be expanded to account for 8 faces instead of 6
	//--------------------------//
	//	current		possible	|	
	//	o--T		o--T--o		|
	//	|\0|\		|\0|7/|		|
	//	|1\|5\		|1\|/6|		|
	//	L--M--R	->	L--M--R		|	M <-calculates normal for this vertex
	//	 \2|\4|		|2/|\5|		|
	//	  \|3\|		|/3|4\|		|
	//	   B--o		o--B--o		|
	//--------------------------//
	glm::vec3 sum(0.0f);	//	The normal of the vector is the sum / average (it's a vector, either works) of the normals of adjacent faces
	int w = size.x, h = size.y;

	Triangle tris[6];	//	All adjacent faces
	values[int(pos.x * w + pos.y)];
	//	Middle
	for (Triangle t : tris) t.A = t.B = t.C = glm::vec3((pos.x - float(h) / 2) / float(h), values[int(pos.x * w + pos.y)], (pos.y - float(w) / 2) / float(w));
	//	Top Left
	if (pos.x != 0 && pos.y != 0) tris[0].C = tris[1].B = glm::vec3((pos.x - 1 - float(h) / 2) / float(h), values[int((pos.x - 1)* w + (pos.y - 1))], (pos.y - 1 - float(w) / 2) / float(w));
	//	Left
	if (pos.x != 0) tris[1].C = tris[2].B = glm::vec3((pos.x - 1 - float(h) / 2) / float(h), values[int((pos.x - 1) * w + pos.y)], (pos.y - float(w) / 2) / float(w));
	//	Bottom
	if (pos.y != w - 1) tris[2].C = tris[3].B = glm::vec3((pos.x - float(h) / 2) / float(h), values[int(pos.x * w + (pos.y+1))], (pos.y + 1 - float(w) / 2) / float(w));
	//	Bottom Right 
	if (pos.x != h - 1 && pos.y != w - 1) tris[3].C = tris[4].B = glm::vec3((pos.x + 1 - float(h) / 2) / float(h), values[int((pos.x + 1) * w + (pos.y + 1))], (pos.y + 1 - float(w) / 2) / float(w));
	//	Right
	if (pos.x != h - 1) tris[4].C = tris[5].B = glm::vec3((pos.x + 1 - float(h) / 2) / float(h), values[int((pos.x + 1) * w + pos.y)], (pos.y - float(w) / 2) / float(w));
	//	Top
	if (pos.y != 0) tris[5].C = tris[0].B = glm::vec3((pos.x - float(h) / 2) / float(h), values[int(pos.x * w + (pos.y - 1))], (pos.y - 1 - float(w) / 2) / float(w));

	//	Sum of all the face normals
	for (Triangle t : tris) sum += FaceNormal(t);
	return sum;
}

Mesh Hmap::ValuesToMesh(GLfloat* heightValues, glm::vec2 size)
{
	Mesh mesh;
	for (int i = 1; i < size.y; i++)
	{
		for (int j = 1; j < size.x; j++)
		{
			//	Vertices
			glm::vec3 brVector((i - float(size.y) / 2) / float(size.y), heightValues[int(i * size.x + j)], (j - float(size.x) / 2) / float(size.x));
			glm::vec3 blVector(((i - 1) - float(size.y) / 2) / float(size.y), heightValues[int((i - 1) * size.x + j)], (j - float(size.x) / 2) / float(size.x));
			glm::vec3 trVector((i - float(size.y) / 2) / float(size.y), heightValues[int(i * size.x + (j - 1))], ((j - 1) - float(size.x) / 2) / float(size.x));
			glm::vec3 tlVector(((i - 1) - float(size.y) / 2) / float(size.y), heightValues[int((i - 1) * size.x + (j - 1))], ((j - 1) - float(size.x) / 2) / float(size.x));

			mesh.vertices.push_back(brVector);	//	Bottom Right
			mesh.vertices.push_back(tlVector);	//	Top Left
			mesh.vertices.push_back(trVector);	//	Top Right
			mesh.vertices.push_back(brVector);	//	Bottom Right
			mesh.vertices.push_back(tlVector);	//	Top Left
			mesh.vertices.push_back(blVector);	//	Bottom Left

													//	Texture coordinates
			glm::vec2 brTex(i / size.y, j / size.x);
			glm::vec2 blTex((i - 1) / size.y, j / size.x);
			glm::vec2 trTex(i / size.y, (j - 1) / size.x);
			glm::vec2 tlTex((i - 1) / size.y, (j - 1) / size.x);

			mesh.texCoord.push_back(brTex);	//	Bottom Right
			mesh.texCoord.push_back(tlTex);	//	Top Left
			mesh.texCoord.push_back(trTex);	//	Top Right
			mesh.texCoord.push_back(brTex);	//	Bottom Right
			mesh.texCoord.push_back(tlTex);	//	Top Left
			mesh.texCoord.push_back(blTex);	//	Bottom Left

												//	Normals
			glm::vec3 brNorm(VertexNormal(heightValues, size, glm::vec2(i, j)));
			glm::vec3 blNorm(VertexNormal(heightValues, size, glm::vec2(i - 1, j)));
			glm::vec3 trNorm(VertexNormal(heightValues, size, glm::vec2(i, j - 1)));
			glm::vec3 tlNorm(VertexNormal(heightValues, size, glm::vec2(i - 1, j - 1)));

			mesh.normals.push_back(brNorm);	//	Bottom Right
			mesh.normals.push_back(tlNorm);	//	Top Left
			mesh.normals.push_back(trNorm);	//	Top Right
			mesh.normals.push_back(brNorm);	//	Bottom Right
			mesh.normals.push_back(tlNorm);	//	Top Left
			mesh.normals.push_back(blNorm);	//	Bottom Left
		}
		//std::cout << "Loaded row: " << i << "\n";
	}
	return mesh;
}

void Hmap::SmoothValues(GLfloat* values, glm::vec2 size)
{
	GLfloat* oldValues = new GLfloat[size.x * size.y];
	*oldValues = *values;
	for (int i = 0; i < size.x; i++)
		for (int j = 0; j < size.y; j++)
		{
			int c = 0;
			float average = 0;
			if (i != 0)
			{
				average += oldValues[int((i - 1) * size.y + j)];
				c++;
			}
			if (i != size.x - 1)
			{
				average += oldValues[int((i + 1) * size.y + j)];
				c++;
			}
			if (j != 0)
			{
				average += oldValues[int(i * size.y + (j - 1))];
				c++;
			}
			if (j != size.y - 1)
			{
				average += oldValues[int(i * size.y + (j - 1))];
				c++;
			}
			values[int(i * size.y + j)] = average / c;
		}
	delete[] oldValues;
}
void Hmap::Squish(Mesh &values)
{
	for (glm::vec3 &v : values.vertices)
		v.y = v.y / amplitude;
	for (glm::vec3 &n : values.normals)
		n.y = n.y * amplitude;
}
void Hmap::GenerateHWater(Mesh &mesh, glm::vec2 size)
{
	GLfloat* waveValues = new GLfloat[size.x * size.y];
	for (int i = 0; i < size.x; i++)
		for (int j = 0; j < size.y; j++)
			waveValues[int(i * size.y + j)] = rand() & 255; // to be noise-ified

	water = new Mesh(ValuesToMesh(waveValues, glm::vec2(size.x, size.y))); 
	Squish(*water);
	mesh = *water;
	//delete[] waveValues;
}

void Hmap::GenerateHMap(Mesh &mesh, const char* path)
{
	//	Read texture data
	unsigned char* data = stbi_load(path, &width, &height, &nrChannels, 0);
	std::string heightString(reinterpret_cast<char*>(data));
	stbi_image_free(data);

	GLfloat* terrainHeight = new GLfloat[width * height];	//	All the height data
	
	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
		{
			float altitude = 0;
			if (i != 0 && j != 0 && i != height - 1 && j != width - 1)
			{
				for (int k = 0; k < nrChannels; k++) altitude += heightString[i * nrChannels * width + j * nrChannels + k];

				altitude = altitude / nrChannels;
			}
			terrainHeight[int(i * width + j)] = altitude;
		}
	//SmoothValues(terrainHeight, glm::vec2(width, height));

	terrain = new Mesh(ValuesToMesh(terrainHeight, glm::vec2(width, height)));

	Squish(*terrain);

	mesh = *terrain;
	//delete[] terrainHeight;
}

